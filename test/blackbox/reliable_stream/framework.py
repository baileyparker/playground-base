from multiprocessing import Process, Manager
# DO NOT IMPORT PLAYGROUND OR TWISTED HERE. Only Import in process to be run.

g_Manager = Manager()
g_Mailbox = g_Manager.dict()

class ChaperoneControl(object):
    def __init__(self, **testArgs):
        self.addr = testArgs.get("addr", "127.0.0.1")
        self.port = testArgs.get("port", 9090)
        self.errorRate = testArgs.get("errorRate", None)
        self.lossRate = testArgs.get("lossRate", None)
        self.process = None
        self.sharedData = g_Manager.dict()
        self.sharedData["RUNNING"] = False
        self.testArgs = testArgs
    
    def running(self):
        return self.process and self.process.is_alive() and self.sharedData["RUNNING"]
    
    def start(self):
        self.process = Process(target=self._realStart, args=(self.errorRate,self.lossRate,self.testArgs))
        self.process.start()
        
    def _realStart(self, errorRate, lossRate, testArgs):
        from playground.network.chaperone import Chaperone
        server = Chaperone(self.addr, self.port)
        if errorRate:
            server.setNetworkErrorRate(*errorRate)
        if lossRate:
            server.setNetworkLossRate(*lossRate)
        if self.testArgs.has_key("playgroundLogging"):
            from playground import playgroundlog
            playgroundlog.Config.loggingNode = "TestThroughputChaperone"
            playgroundlog.EnablePresetLogging(testArgs["playgroundLogging"])
        self.sharedData["RUNNING"] = True
        server.callLater(1.0,lambda: self.__monitorServer(server))
        server.run()
        print "setting stats"
        # we can't copy server back to the other side. If it takes
        # the reactor with it, it causes all kinds of havoc
        
        self.sharedData["STATISTICS"] = server.statistics().__dict__
    
    def __monitorServer(self, server):
        if not self.sharedData.get("RUNNING",False):
            server.stop()
        server.callLater(1.0, lambda: self.__monitorServer(server))
    
    def stop(self):
        self.sharedData["RUNNING"] = False
        #self.process.terminate()
        
    def join(self):
        #pass
        self.process.join()
        #self.process.terminate()
        
    def statistics(self):
        s = self.sharedData.get("STATISTICS",None)
        return s
    
class GateControl(object):
    def __init__(self, connectionData, **testArgs):
        self.connectionData = connectionData
        self.process = None
        self.testArgs = testArgs
        
    def running(self):
        return self.process and self.process.is_alive()
        
    def start(self):
        self.process = Process(target=self._realStart, args=(self.connectionData, self.testArgs))
        self.process.start()
        
    def _realStart(self, connectionData, testArgs):
        import sys
        modkeys = sys.modules.keys()
        #print modkeys
        for moduleName in modkeys:
            if "twisted" in moduleName or "playground" in moduleName:
                print "WARNING", moduleName
        i = 1

        from playground.network.gate import Service
        #from playground.twisted.error.ErrorHandlers import TwistedShutdownErrorHandler
        from twisted.internet import reactor
        from playground.twisted.error.ErrorHandlers import TwistedShutdownErrorHandler
        if self.testArgs.has_key("playgroundLogging"):
            from playground import playgroundlog
            playgroundlog.Config.loggingNode = "TestThroughputGate_%d" % connectionData.gatePort
            playgroundlog.EnablePresetLogging(testArgs["playgroundLogging"])
        #logctx = playgroundlog.LoggingContext("GATE_%s" % gateConfig.playgroundAddr)
    
        # Uncomment the next line to turn on "packet tracing"
        #logctx.doPacketTracing = True
    
        #playgroundlog.startLogging(logctx)
        #playgroundlog.UseStdErrHandler(True)

        TwistedShutdownErrorHandler.HandleRootFatalErrors()
        Service.Create(reactor, connectionData)
        print "start gate"
        reactor.run()
        print 'stop gate'
        
    def stop(self):
        self.process.terminate()
        
    def join(self):
        self.process.join()
    
class TestPeer(object):
    BROADCAST_TEST_ID = "__broadcast__"
    def __init__(self, testId, **testArgs):
        self.__testId = testId
        self.__mailbox = g_Mailbox
        self.__mailbox[testId] = g_Manager.list()
        self.process = None
        self.__sharedData = g_Manager.dict()
        self.testArgs = testArgs
        
    def _realStart(self, *args):
        raise Exception("Re-implement in subclass")
    
    def start(self, *args):
        argsList = list(args)
        argsList.append(self.testArgs)
        self.process = Process(target=self._realStart, args=argsList)
        self.process.start()
        
    def stop(self):
        self.process.terminate()
        
    def join(self):
        self.process.join()
        
    def testId(self):
        return self.__testId
    
    def sendTestData(self, dstTestId, data):
        if dstTestId == self.BROADCAST_TEST_ID:
            txIds = self.__mailbox.keys()
        else:
            txIds = [dstTestId]
        for testId in txIds:
            if not self.__mailbox.has_key(testId):
                self.__mailbox[testId] = g_Manager.list()
            l = self.__mailbox[testId]
            l.append((self.__testId, data))
            self.__mailbox[testId] = l
    
    def getTestData(self):
        if not self.__mailbox[self.__testId]:
            return None
        return self.__mailbox[self.__testId].pop()
    
    def setSharedData(self, k, v):
        self.__sharedData[k] = v
        
    def sharedData(self, k, default=None):
        return self.__sharedData.get(k, default)