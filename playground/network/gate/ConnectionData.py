'''
Created on Sep 20, 2016

@author: sethjn
'''

from playground.network.common import PlaygroundAddress

class ConnectionData(object):
    
    def __init__(self, 
                 chaperoneAddr="127.0.0.1", 
                 chaperonePort=9090, 
                 gatePort=9091, 
                 gateAddr="*.*.*.*"):
        self.chaperoneAddr = chaperoneAddr
        self.chaperonePort = int(chaperonePort)
        self.playgroundAddr = gateAddr
        self.gatePort = int(gatePort)
        