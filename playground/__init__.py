# locate Playground configuration

import os, sys
PLAYGROUND_BASE_DIRECTORY = os.path.abspath(os.path.dirname(__file__))
SRC_ROOT = os.path.abspath(os.path.join(PLAYGROUND_BASE_DIRECTORY, ".."))

import playgroundlog
import network as network
import crypto
