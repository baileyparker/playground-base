'''
Created on Dec 5, 2013

@author: sethjn
'''

# This import needed to force twisted to be the real twisted, and not
# playground.twisted
from __future__ import absolute_import

import logging, logging.handlers
import random, os, sys
from argparse import Action
from twisted.python import log

class TaggedLogger(object):
    TAG_KEY = "__playlogtag__"
        
    
    @classmethod
    def GetTag(cls, loggerName):
        parts = loggerName.split(".")
        if parts[-1].startswith(cls.TAG_KEY):
            return parts[-1][len(cls.TAG_KEY):]
        return ""
    
    @classmethod
    def GetTaggedLoggerName(cls, baseLoggerName, tag):
        return ".".join([baseLoggerName, cls.TAG_KEY+tag])
    
    @classmethod
    def GetTaggedLoggerNameForObject(cls, obj, tag):
        return cls.GetTaggedLoggerName(".".join([obj.__module__, obj.__class__.__name__]), tag)
        

class PlaygroundLoggingFilter(logging.Filter):
    LOG_ALL_MODULES = 'global'
    LOG_ALL_TAGS = 'verbose'
    
    def __init__(self, logModules=LOG_ALL_MODULES, logTags=LOG_ALL_TAGS):
        self.configure(logModules, logTags)
        
    def configure(self, logModules=LOG_ALL_MODULES, logTags=LOG_ALL_TAGS):
        if logModules == self.LOG_ALL_MODULES or not logModules:
            self.globalLogging = True
            self.moduleLogging = []
        else:
            self.globalLogging = False
            self.moduleLogging = logModules
            
        self.tagLogging = logTags

    def logThisTag(self, tagName):
        if self.tagLogging == self.LOG_ALL_TAGS or not tagName: return True
        if not self.tagLogging: return False
        return tagName in self.tagLogging
        
    def logThisLogger(self, loggerName):
        loggerTag = TaggedLogger.GetTag(loggerName)
        
        if self.globalLogging: 
            return self.logThisTag(loggerTag)
        for loggedModuleName in self.moduleLogging:
            if loggerName.startswith(loggedModuleName):
                
                return self.logThisTag(loggerTag)
        return False
    
    def filter(self, record):
        return self.logThisLogger(record.name)

class PartialConverter(object):
    def __init__(self, d):
        self.d =d 
        
    def __getitem__(self, k):
        if self.d.has_key(k): return self.d[k]
        return "%("+k+")s"

class PlaygroundLoggingFormatter(logging.Formatter):
    SPECIAL_CONVERTERS = {}
    def __init__(self, fmt="%(asctime)s - %(name)s - %(levelname)s - %(message)s"):
        logging.Formatter.__init__(self, fmt)
        
    def format(self, record):
        if hasattr(record, '__playground_special__'):
            converters = {}
            for dataKey, specialData in record.__playground_special__.items():
                converters[dataKey] = self.SPECIAL_CONVERTERS[dataKey](specialData)
            print record.msg, converters
            record.msg = record.msg % PartialConverter(converters)
        return super(PlaygroundLoggingFormatter, self).format(record)
        

class PlaygroundLoggingConfiguration(logging.Handler):
    
    PLAYGROUND_ROOT_LOGGER = "playground"
    
    STDERR_HANDLER = logging.StreamHandler()
    
    # LOGFILE is defined in the singleton constructor
    
    def __init__(self):
        super(logging.Handler, self).__init__()
        self.loggingNode = sys.argv and str(sys.argv[0]) or "unknown playground_app"
        self.handlers = {}        
        # rootLogLevel is set every time enableLogging is called.
        # Logger logs everyting. We filter at the hander/filter level
        
    def toggleMaxLogging(self):
        raise Exception("Not yet implemented") 
        
    def handle(self, record):
        for handler in self.handlers:
            if handler.level <= record.levelno:
                handler.handle(record)
            
    def enableLogging(self, logLevel=logging.NOTSET, enableTwisted=True, rootLogging=False):
        "by default, turn all logging on an leave to handlers to filter"
        
        if rootLogging:
            rootLogger = ""
        else:
            rootLogger = self.PLAYGROUND_ROOT_LOGGER
            "Python is weird about 'NOTSET'. It enables everything at root, but"
            "for all other loggers, just propagates. So we'll just set it to '1' which"
            "is, I belive, the highest logging other than NOTSET"
            if logLevel == logging.NOTSET:
                logLevel = 1
            
        logging.getLogger(rootLogger).setLevel(logLevel)
        if self not in logging.getLogger(rootLogger).handlers:
            logging.getLogger(rootLogger).addHandler(self)
            
        # this handler logs everyting, and then passes off to sub-handlers
        self.setLevel(logging.NOTSET)
        if enableTwisted:
            observer = log.PythonLoggingObserver("playground.__twisted__")
            log.startLoggingWithObserver(observer.emit, setStdout=False)
    
    def enableHandler(self, handler, level=logging.DEBUG, specificModules=None, specificTags=None):
        #TODO: User should be able to configure formatter somehow, but it
        # needs to be a playground formatter.
        handler.setLevel(level)
        handler.setFormatter(PlaygroundLoggingFormatter())
        if not self.handlers.has_key(handler):
            pfilter = PlaygroundLoggingFilter(specificModules, specificTags)
            handler.addFilter(pfilter)
            self.handlers[handler] = pfilter
        else:
            pfilter = self.handlers[handler]
            pfilter.configure(specificModules, specificTags)
    
    def disableHandler(self, handler):
        if self.handlers.has_key(handler):
            handler.removeFilter(self.handlers[handler])
            del self.handlers[handler]
            
    def createRotatingLogFileHandler(self, name=None, path=None):
        if not path:
            path = os.path.join( os.getcwd(), "playlogs")
            if not os.path.exists(path):
                os.mkdir(path)
        if not name:
            name = os.path.basename(self.loggingNode) +".log"
        return logging.handlers.TimedRotatingFileHandler(os.path.join(path, name))
        
        
Config = PlaygroundLoggingConfiguration() 

PRESET_MINIMAL = "MINIMAL"
PRESET_QUIET = "QUIET"
PRESET_DEBUG = "DEBUG"
PRESET_VERBOSE = "VERBOSE"
PRESET_LOUD = "LOUD"
PRESET_MAX = "MAX"
PRESET_LEVELS = [PRESET_MINIMAL,
                 PRESET_QUIET,
                 PRESET_DEBUG,
                 PRESET_VERBOSE,
                 PRESET_LOUD,
                 PRESET_MAX,
                 ]

def EnablePresetLogging(level, rootLogging=False):
    if level == PRESET_MINIMAL:
        # log info messages to logfile
        Config.enableLogging(logLevel=logging.ERROR, 
                             enableTwisted=False, 
                             rootLogging=rootLogging)
        Config.enableHandler(Config.createRotatingLogFileHandler())
    elif level == PRESET_QUIET:
        # logs most messages, but only to logfile
        Config.enableLogging(logLevel=logging.DEBUG, 
                             enableTwisted=False,
                             rootLogging=rootLogging)
        Config.enableHandler(Config.createRotatingLogFileHandler())
    elif level == PRESET_DEBUG:
        # logs all messages including those from twisted
        Config.enableLogging(logLevel=logging.NOTSET, 
                             enableTwisted=True,
                             rootLogging=rootLogging)
        Config.enableHandler(Config.createRotatingLogFileHandler())
    elif level == PRESET_VERBOSE:
        Config.enableLogging(logLevel=logging.NOTSET, 
                             enableTwisted=True,
                             rootLogging=rootLogging)
        Config.enableHandler(Config.STDERR_HANDLER, level=logging.ERROR)
        Config.enableHandler(Config.createRotatingLogFileHandler())
    elif level == PRESET_LOUD:
        Config.enableLogging(logLevel=logging.NOTSET, 
                             enableTwisted=True,
                             rootLogging=rootLogging)
        Config.enableHandler(Config.STDERR_HANDLER)
    elif level == PRESET_MAX:
        raise Exception("Not yet implemented. Maybe will remove.")
    else:
        raise Exception("Unknown preset log level %s" % level)
    
def CmdLineToLogging(cmdLineArg):
    if cmdLineArg == "v"*len(cmdLineArg):
        EnablePresetLogging(PRESET_LEVELS[len(cmdLineArg)])
    else:
        EnablePresetLogging(cmdLineArg)
        
def ConfigureArgParser(parser, default="MINIMAL", rootLogging=False):
    class ParserAction(Action):
        def __init__(self, option_strings, dest, nargs=None, **kwargs):
            if nargs is not None:
                raise ValueError("nargs not allowed")
            super(ParserAction, self).__init__(option_strings, dest, **kwargs)
            self.rootLogging = rootLogging
        def __call__(self, parser, namespace, values, option_string=None):
            setattr(namespace, self.dest, values)
            print "configure with rootLogging as", self.rootLogging
            EnablePresetLogging(values, rootLogging=self.rootLogging)
            
    if rootLogging:
        longName = "--logging"
        help = "Enable logging. Default values are %s" % PRESET_LEVELS,
    else:
        longName = "--playground-logging"
        help = "Enable logging for the playground framework. Default values are %s" % PRESET_LEVELS,
        
    parser.add_argument(longName,
                        dest="playground_logging",
                        default=default,
                        help=help,
                        action=ParserAction)
    if default != None:
        EnablePresetLogging(default, rootLogging)
