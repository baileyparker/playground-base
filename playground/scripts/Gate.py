'''
Created on Sep 10, 2016

@author: sethjn
'''

from playground import playgroundlog
from playground.network.gate import Service, ConnectionData
from playground.twisted.error.ErrorHandlers import TwistedShutdownErrorHandler
from twisted.internet import reactor
import sys
from playground.network.common.PlaygroundAddress import PlaygroundAddressBlock,\
    PlaygroundAddress

def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--chaperone", default="127.0.0.1:9090")
    parser.add_argument("--port", default="9091")
    parser.add_argument("--default-address", default="*.*.*.*")
    playgroundlog.ConfigureArgParser(parser, default="DEBUG", rootLogging=True)
    
    opts = parser.parse_args()
    
    try:
        chaperoneAddr, chaperonePort = opts.chaperone.split(":")
        chaperonePort = int(chaperonePort)
    except:
        sys.exit("Chaperone argument must be of the form <ip address>:<port>")
    try:
        gatePort = int(opts.port)
        if gatePort < 0:
            raise Exception()
    except:
        sys.exit("Gate port must be a positive integer")

    if "*" in opts.default_address:
        anyBlock = PlaygroundAddressBlock.FromString(opts.default_address)
        gateAddress = anyBlock.spawnAddress()
    else:
        gateAddress = PlaygroundAddress.FromString(opts.default_address)
    print "Gate initializing with default playground address", gateAddress
    
    g2gConnect = ConnectionData(chaperoneAddr, chaperonePort, gatePort, gateAddress)
    
    TwistedShutdownErrorHandler.HandleRootFatalErrors()
    Service.Create(reactor, g2gConnect)
    reactor.run()
    
if __name__=="__main__":
    main()